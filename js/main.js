let danhSachSinhVien = [];

let validatorSv = new ValidatorSV();

const DSSV_LOCALSTORAGE = "DSSV_LOCALSTORAGE";

const timKiemViTri = function (id, array) {
  return array.findIndex(function (sv) {
    return sv.maSv == id;
  });
};

const luuLocalStorage = function () {
  let dssvJson = JSON.stringify(danhSachSinhVien);

  localStorage.setItem(DSSV_LOCALSTORAGE, dssvJson);
};

// lấy dữ liệu từ localStorage khi user tải lại trang

var dssvJson = localStorage.getItem(DSSV_LOCALSTORAGE);

//  gán cho array gốc và render lại giao diện
if (dssvJson) {
  danhSachSinhVien = JSON.parse(dssvJson);

  danhSachSinhVien = danhSachSinhVien.map(function (item) {
    return new SinhVien(
      item.maSV,
      item.tenSV,
      item.emailSV,
      item.diemToan,
      item.diemLy,
      item.diemHoa
    );
  });
  xuatDanhSachSinhVien(danhSachSinhVien);
}

function themSinhVien() {
  var newSinhVien = layThongTinTuForm();

  var isValid = true;

  var index = danhSachSinhVien.findIndex(function (item) {
    return item.maSv == newSinhVien.maSv;
  });

  

  var isValidMaSV =
    validatorSv.kiemTraRong(
      "txtMaSV",
      "spanMaSV",
      "Mã sinh viên không được để rỗng"
    ) && validatorSv.kiemTraIdHopLe(newSinhVien, danhSachSinhVien);

  var isValidEmail =
    validatorSv.kiemTraRong(
      "txtEmail",
      "spanEmailSV",
      "Email không được để rỗng"
    ) && validatorSv.kiemTraEmail("txtEmail", "spanEmailSV");

  var isValidTenSV =
    validatorSv.kiemTraRong(
      "txtTenSV",
      "spanTenSV",
      "Tên sinh viên không được để rỗng"
    ) && validatorSv.kiemTraTenHopLe("txtTenSV", "spanTenSV");

  var isValid = isValidMaSV && isValidEmail && isValidTenSV;
  if (isValid) {
    danhSachSinhVien.push(newSinhVien);
    xuatDanhSachSinhVien(danhSachSinhVien);
    // convert array thành json để có thể lưu vào localStorage
    luuLocalStorage();
  }
}

function xoaSinhVien(id) {
  // console.log(id);
  var viTri = timKiemViTri(id, danhSachSinhVien);
  // console.log({ viTri });
  // xoá tại ví trị tìm thấy với số lượng là 1
  danhSachSinhVien.splice(viTri, 1);
  xuatDanhSachSinhVien(danhSachSinhVien);
  luuLocalStorage();
}

function suaSinhVien(id) {
  var viTri = timKiemViTri(id, danhSachSinhVien);
  console.log({ viTri });

  var sinhVien = danhSachSinhVien[viTri];

  xuatThongTinLenForm(sinhVien);
}
function capNhatSinhVien() {
  var sinhVienEdit = layThongTinTuForm();
  console.log({ sinhVienEdit });

  let viTri = timKiemViTri(sinhVienEdit.maSv, danhSachSinhVien);

  danhSachSinhVien[viTri] = sinhVienEdit;

  xuatDanhSachSinhVien(danhSachSinhVien);

  luuLocalStorage();
}

function resetSinhVien() {
  document.getElementById("txtMaSV").value = "";
  document.getElementById("txtTenSV").value = "";
  document.getElementById("txtEmail").value = "";
  document.getElementById("txtDiemToan").value = "";
  document.getElementById("txtDiemLy").value = "";
  document.getElementById("txtDiemHoa").value = "";
}

function timKiemSinhVien() {
  let tenSVTimKiem = document.getElementById("txtSearch").value;
  let danhSachTheoTen = danhSachSinhVien.filter(function (item) {
    return item.tenSv == tenSVTimKiem;
  });
  xuatDanhSachSinhVien(danhSachTheoTen);
}
